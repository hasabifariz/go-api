package main

import (
	"fmt"
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	//Connect to Database
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("DB Connection Error")
	}
	fmt.Println("Database Connected")
	//migrate
	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	router := gin.Default()
	//versioning API is needed when there is a change to struct. Better to make a new version group/endpoint if there is a change.
	//v1
	version1 := router.Group("/v1")
	//books
	version1.GET("/books", bookHandler.GetListBook)
	version1.GET("/books/:id", bookHandler.GetBook)
	version1.POST("/books", bookHandler.CreateBook)
	version1.PUT("/books/:id", bookHandler.UpdateBook)
	version1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run(":8081")
}