package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"pustaka-api/book"
)

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler {
	return &bookHandler{bookService}
}

//GET LIST
func (handler *bookHandler) GetListBook(c *gin.Context) {
	books, err := handler.bookService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	var booksRes []book.BookResponse
	for _, b := range books {
		bookResponse := convertToBookResponse(b)
		booksRes = append(booksRes, bookResponse)
	}
	c.JSON(http.StatusOK, gin.H{
		"data": booksRes,
	})
}

//GET BY ID
func (handler *bookHandler) GetBook(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	b, err := handler.bookService.FindById(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": "There is no such data with id: "+ param,
		})
		return
	}
	bookRes := convertToBookResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookRes,
	})
}

//POST
func (handler *bookHandler) CreateBook(c *gin.Context) {
	var bookRequest book.BookRequest
	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		eMessage := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMsg := fmt.Sprintf("Error on field : %s, condition : %s", e.Field(), e.ActualTag())
			eMessage = append(eMessage, errorMsg)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": eMessage,
		})
		return
	}
	book, err := handler.bookService.Create(bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

//PUT
func (handler *bookHandler) UpdateBook(c *gin.Context) {
	var bookRequest book.BookRequest
	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		eMessage := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMsg := fmt.Sprintf("Error on field : %s, condition : %s", e.Field(), e.ActualTag())
			eMessage = append(eMessage, errorMsg)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": eMessage,
		})
		return
	}
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	book, err := handler.bookService.Update(id, bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}
//DELETE
func (handler *bookHandler) DeleteBook(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	b, err := handler.bookService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": "There is no such data with id: "+ param ,
		})
		return
	}
	bookRes := convertToBookResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookRes,
	})
}

func convertToBookResponse(b book.Book) book.BookResponse {
	return book.BookResponse{
		ID:          b.ID,
		Title:       b.Title,
		Description: b.Description,
		Price:       b.Price,
		Rating:      b.Rating,
	}
}
