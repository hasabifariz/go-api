package book

type Service interface {
	FindAll() ([]Book, error)
	FindById(ID int) (Book, error)
	Create(book BookRequest) (Book, error)
	Update(ID int, book BookRequest) (Book, error)
	Delete(ID int) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindById(ID int) (Book, error) {
	book, err := s.repository.FindById(ID)
	return book, err
}

func (s *service) Create(bookreq BookRequest) (Book, error) {
	price, _ := bookreq.Price.Int64()
	book := Book{
		Title:       bookreq.Title,
		Description: bookreq.Description,
		Rating:      bookreq.Rating,
		Price:       int(price),
	}
	newBook, err := s.repository.Create(book)
	return newBook, err
}

func (s *service) Update(ID int, bookreq BookRequest) (Book, error) {
	b, err := s.repository.FindById(ID)
	price, _ := bookreq.Price.Int64()

	b.Title = bookreq.Title
	b.Description = bookreq.Description
	b.Rating = bookreq.Rating
	b.Price = int(price)

	newBook, err := s.repository.Update(b)
	return newBook, err
}

func (s *service) Delete(ID int) (Book, error) {
	b, err := s.repository.FindById(ID)
	book, err := s.repository.Delete(b)
	return book, err
}
